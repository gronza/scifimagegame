﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorMove : MonoBehaviour {

    public static bool PlayerIsOnTop = false;
    

    void Start()
    {
        PlayerIsOnTop = this;
    }

   

    void Update()
    {
        while (PlayerIsOnTop)
        {
            
            transform.Translate(Vector3.up * Time.deltaTime, Space.World);
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        

        if (other.gameObject.CompareTag("Player"))
        {

            PlayerIsOnTop = true;

        }

       

    }


}
