﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine;


public class VignetteControl : MonoBehaviour {
    [Range(0, 1)]
    public float _VignetteIntensity;


    PostProcessVolume volume;
    Vignette vignette = null;
	// Use this for initialization
	void Start () {
        volume = GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings(out vignette);
        
	}
	
	// Update is called once per frame
	void Update () {
        vignette.opacity.value = _VignetteIntensity;
	}
}
