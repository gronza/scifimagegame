﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemy : MonoBehaviour {

    public PlayerShoot shooter;
    public PlayerShoot player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Fireball")) {
            Destroy(this.gameObject);
            //Destroy(other.gameObject);

            shooter.addScore(Random.Range(3000, 5000));

        }
    }
}
