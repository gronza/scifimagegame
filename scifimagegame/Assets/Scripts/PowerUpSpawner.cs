﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour {

    public Transform[] PowerUpSpawns;
    

    int amountOfPickUps = 5;
    

    public int i = 0;

    public GameObject PowerUp;

    Transform spawn;

   float SpawnTimer;

    public int PickedUpAmount;



    public void SpawnPowerUp()
    {


        List<Transform> freeSpawnPoints = new List<Transform>(PowerUpSpawns);

       
            for (i = 0; i < amountOfPickUps; i++)

            {
                if (freeSpawnPoints.Count <= 0)
                {



                    return; // Not enough spawn points
                }



                int index = Random.Range(0, freeSpawnPoints.Count);

                Transform pos = freeSpawnPoints[index];

                freeSpawnPoints.RemoveAt(index); // remove the spawnpoint from our temporary list
                Instantiate(PowerUp, pos.position, pos.rotation);


            

        }
        

    
            }
        


    // Use this for initialization
    void Start () {
            
            SpawnTimer = 0;
            SpawnPowerUp();


    }
	
	// Update is called once per frame
	void Update () {

        

            SpawnTimer += Time.deltaTime;
        //Debug.Log(SpawnTimer);
        
        if(SpawnTimer >= 5)
        {
            SpawnTimer = 0;
            
        }

        //Debug.Log(PickedUpAmount);

       

        if(PickedUpAmount >= 5)
        {
                PickedUpAmount = 0;
                SpawnPowerUp();
                
            
        }
        
    }
        

    }

