﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A script used to randomy spawn mobs on selected spawnpoints
public class MobSpawner : MonoBehaviour {

    public GameObject enemy;

    public Transform spawnPoint1;
    public Transform spawnPoint2;
    public Transform spawnPoint3;
    public Transform spawnPoint4;
    public Transform spawnPoint5;

    public Transform[] spawnPoints;

    Transform spawn;

    float spawnTimer;
    public int spawnCount;

    // Use this for initialization
    void Start () {

        spawnTimer = 5;
        spawnCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
        spawnTimer -= Time.deltaTime;

        if (spawnTimer <= 0 && spawnCount <= 5) {
            int spawnNumber = Random.Range(0, 5);

            spawn = spawnPoints[spawnNumber];

            Instantiate(enemy, spawn.position,
                spawn.rotation);

            spawnTimer = Random.Range(3, 4);
            spawnCount++;
        }

	}
}
