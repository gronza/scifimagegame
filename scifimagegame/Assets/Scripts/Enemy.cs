﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AudioSource))]
public class Enemy : MonoBehaviour
{

    public Transform goal;
    NavMeshAgent agent;
    Animator anim;
    int attack;
    private AudioSource m_AudioSource;
    [SerializeField] private AudioClip[] robotsteps;
    float stepTimer = 0;
    public float health;

    float damageTimer;
    public PlayerShoot player;

    public bool alive;

    public ParticleSystem deathAnimation;

    public float spawnTimer;
    public MobSpawner spawner;
    public AudioClip deathSound;

    // Use this for initialization
    void Start()
    {

        spawner = GameObject.Find("Spawners").GetComponent<MobSpawner>();


        alive = true;
        health = 2;

        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        m_AudioSource = GetComponent<AudioSource>();

        damageTimer = 0.7f;

        goal = GameObject.FindGameObjectWithTag("Player").transform;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerShoot>();

        
        deathAnimation = GetComponentInChildren<deathscripti>().gameObject.GetComponent<ParticleSystem>();
        deathAnimation.Stop();


        //SPAWN ANIMATION
        
        GetComponentInChildren<vihuscripti>().gameObject.
                    GetComponent<SkinnedMeshRenderer>().enabled = false;

        GetComponentInChildren<miekka>().gameObject.
            GetComponent<SkinnedMeshRenderer>().enabled = false;
            
        spawnTimer = 2.5f;

        Destroy(GameObject.FindGameObjectWithTag("Spawn"), 2.5f);
    }


    void RoboWalk()
    {
        //play stepparino sounds when robotterino moves
        int n = Random.Range(1, robotsteps.Length);
        m_AudioSource.clip = robotsteps[n];
        
        //timer for walkerino
        

        if(stepTimer >= 0.4f)
        {
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            stepTimer = 0;
        }
           
       
        // move picked sound to index 0 so it's not picked next time
        robotsteps[n] = robotsteps[0];
        robotsteps[0] = m_AudioSource.clip;
        
    }

    void DeathSound()
    {
        m_AudioSource.volume = 0.5f;
        m_AudioSource.clip = deathSound;
        m_AudioSource.PlayOneShot(m_AudioSource.clip);


    }


    // Update is called once per frame
    void Update()
    {

        if (alive)
        {
            SetAlive();
        }

        if (spawnTimer > 0)
        {
            spawnTimer -= Time.deltaTime;

            if (alive)
            {
                if (spawnTimer <= 0)
                {
                    GetComponentInChildren<vihuscripti>().gameObject.
                        GetComponent<SkinnedMeshRenderer>().enabled = true;

                    GetComponentInChildren<miekka>().gameObject.
                        GetComponent<SkinnedMeshRenderer>().enabled = true;

                }
            }
        }

        attack = Random.Range(1, 10);
        float dist = Vector3.Distance(goal.position, transform.position);


        stepTimer += Time.deltaTime;



        //Debug.Log(dist);

        

        if (dist <= 3.5 && alive && spawnTimer < 0)
        {
            //Debug.Log("Robot is alive!");

            agent.isStopped = true;

            //Attack animation
            anim.SetBool("InRange", true);
            if (attack < 6)
            {
                anim.SetInteger("Attack", 0);
            }
            else
            {
                anim.SetInteger("Attack", 1);
            }

            //Attack damage
            damageTimer += Time.deltaTime;
            if (damageTimer >= 1)
            {
                damageTimer = 0;
                player.TakeDamage(1);

            }
        }
        else if(alive && spawnTimer <= 0)
        {

            damageTimer = 0.7f;

            agent.isStopped = false;
            agent.destination = goal.position;
            anim.SetBool("Target", true);
            anim.SetBool("InRange", false);
            RoboWalk();
            
        }

        if (!alive)
        {
            //Debug.Log("Robot is deded :(");
            agent.isStopped = true;

            GetComponentInChildren<vihuscripti>().gameObject.
                    GetComponent<SkinnedMeshRenderer>().enabled = false;

            GetComponentInChildren<miekka>().gameObject.
                GetComponent<SkinnedMeshRenderer>().enabled = false;


            if (!deathAnimation.isPlaying)
            {
                deathAnimation.Play();
            }

            agent.enabled = false;

            Destroy(gameObject, 3);
            
        }

        //SetAlive();

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Fireball"))
        {
            Destroy(this.gameObject);
            //Destroy(other.gameObject);

            player.addScore(Random.Range(3000, 5000));

        }
    }

    public void SetAlive()
    {

        if (health == 1) {
            //GetComponent<Texture>().anisoLevel = 0;
        }

        if (health <= 0)
        {
            //alive = false;
            DeathSound();
        }
    }

    public void TakeDamage(float f) {
        if (spawnTimer <= 0)
        {
            health = health - f;
        }

        if(health <= 0)
        {
            if (alive) {
                DeathSound();
            }

            player.addScore(Random.Range(3000, 5000));
            alive = false;
            spawner.spawnCount--;
            agent.isStopped = true;
            agent.speed = 0;

        }
    }
}
