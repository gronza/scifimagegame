﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadHit : MonoBehaviour {

    public PlayerShoot player;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerShoot>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Fireball"))
        {


            if (GetComponentInParent<Enemy>().alive)
                    {
                        other.gameObject.SetActive(false);
                    }

            GetComponentInParent<Enemy>().TakeDamage(2);
            

        }
        else

        if (other.gameObject.CompareTag("SuperFireball"))
        {

            if (GetComponentInParent<Enemy>().alive)
            {
                other.gameObject.SetActive(false);
            }

            GetComponentInParent<Enemy>().TakeDamage(2);

            
        }
    }
}
