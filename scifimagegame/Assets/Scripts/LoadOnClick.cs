﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadOnClick : MonoBehaviour {

    //load scene on click
    public void LoadByIndex(int sceneIndex)
    {

        if (sceneIndex == 2) {
            Application.Quit();
        }

        SceneManager.LoadScene(sceneIndex);

        
    }


}
