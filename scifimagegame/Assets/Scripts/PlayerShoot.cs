﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(AudioSource))]
public class PlayerShoot : MonoBehaviour {

    public GameObject fireBallPrefab;
    public Transform fireBallSpawn;

    public Transform spawnPoint;

    [SerializeField] private AudioClip[] attackSound;
    [SerializeField] private AudioClip[] damageToPlayer;
    [SerializeField] private AudioClip regenSound;
    [SerializeField] private AudioClip deathSound;
    [SerializeField] private AudioClip[] painSound;
    [SerializeField] private AudioClip lowHealthSound;



    public GameObject weapon;
    public GameObject hitBox;

    private AudioSource m_AudioSource;
    private AudioSource m2_AudioSource;

    public float chargeTimer;
    public float coolDown;
    public float fireBallDamage;

    public float swingTimer;
    public bool swinging;
    public bool swingingBack;
    public bool forward;
    public float forwardTimer;
    public Vector3 origPos;
    public Quaternion origRot;

    public int health;
    public float regenTimer;
    public bool alive;


    public GameObject Wall;

    public float score;
    public Text scoreText;
    public Text winText;
    public Image winScreen;

    Rigidbody fireball_rigidbody;

    public GameObject endCamera;

    public float timer;

    //POWERUPS
    public float powerUpTimer;
    public float swingSpeed;

    public bool powerUpSpeed;
    public bool powerUpDamage;
    public bool powerUpShield;
    public bool powerUpAttackSpeed;

    //Shot sound
    public GameObject shotSoundSrc;
    public GameObject chargeSound1;
    bool charge1Spawned = false;
    GameObject charge1;


    //VIGNETTE CONTROLS
    [Range(0, 1)]
    public float _VignetteIntensity;

    public PostProcessVolume volume;
    Vignette vignette = null;

    // Use this for initialization
    void Start() {
        chargeTimer = 0.2f;
        coolDown = 0;
        fireBallDamage = 1;
        timer = 0;

        alive = true;

        transform.SetPositionAndRotation(new Vector3(spawnPoint.transform.position.x, spawnPoint.transform.position.y, spawnPoint.transform.position.z),
                        new Quaternion());

        score = 0;
        scoreText.text = "Score: " + score;
        swinging = false;
        swingTimer = 0;
        //origPos = new Vector3(this.transform.position.x , this.transform.position.y, this.transform.position.z);
        origRot = weapon.transform.rotation;
        hitBox.SetActive(false);
        forwardTimer = 0;

        m_AudioSource = GetComponent<AudioSource>();
        m2_AudioSource = GetComponent<AudioSource>();

        //POWERUPS
        powerUpTimer = 0;

        swingSpeed = 20;

        powerUpSpeed = false;
        powerUpDamage = false;
        powerUpShield = false;
        powerUpAttackSpeed = false;

        health = 3;
        regenTimer = 5;

        //VIGNETTE CONTROLS
        //volume = GetComponentInChildren<PostProcessVolume>();
        volume.profile.TryGetSettings(out vignette);
    }

    // Update is called once per frame
    void Update() {

        origPos = new Vector3(this.transform.position.x - 0.015f, this.transform.position.y - 0.489f, this.transform.position.z + 0.587f);
        if (!swinging)
        {
            //origRot = weapon.transform.rotation;
        }

        if (alive)
        {
            PowerUp();
            Fire();
            Melee();
            ResetPlayer();
            VignetteControl();
        }


        if (timer > 0 && !alive)
        {
            timer = timer - Time.deltaTime;
            if (timer < 0.5f)
            {
                winScreen.CrossFadeAlpha(0, 5, false);
         
                winText.gameObject.SetActive(true);

                scoreText.gameObject.SetActive(false);

                if (GetComponentInChildren<Camera>().gameObject.active == true)
                {
                    GetComponentInChildren<Camera>().gameObject.SetActive(false);
                }
                endCamera.SetActive(true);

                
            }

        }
    



    Debug.Log("Updating");
        if (!alive) {

            Debug.Log("We ded now");
            if (Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("We press R");
                LoadByIndex(1);
            }
            else if (Input.GetKeyDown(KeyCode.M))
            {
                Debug.Log("We press M");
                LoadByIndex(0);
                Cursor.visible = true;
            }
        }

    }



    void Fire()
    {

        coolDown -= Time.deltaTime;
        

        if (Input.GetMouseButton(0) && !swinging)
        {

            if (coolDown < 0)
            {
                chargeTimer += Time.deltaTime / 2;
                fireBallDamage += Time.deltaTime;

                Vector3 swing = new Vector3(0.01f, -0.05f, -0.05f);


                //Charge sound stuff
                if (!charge1Spawned) {
                    charge1 = Instantiate(chargeSound1, fireBallSpawn.position, Quaternion.identity);
                    charge1Spawned = true;
                }
                


                //Spawning a temporary fireball size indicator
                var fireBall = (GameObject)Instantiate(
                                        fireBallPrefab,
                                        fireBallSpawn.position,
                                        fireBallSpawn.rotation
                                        );

                //disabling the temp fireball
                fireBall.GetComponent<BoxCollider>().enabled = false;

                //Adjusting fireball size
                /*
                if (chargeTimer < 0.2f)
                {
                    fireBall.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                }
                */

                if (chargeTimer > 0.0f && chargeTimer < 0.5f)
                {
                    fireBall.transform.localScale = new Vector3(chargeTimer, chargeTimer, chargeTimer);
                    weapon.transform.Rotate(swing);
                }


                if (chargeTimer > 0.5f)
                {

                    fireBall.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    if (forward) {

                        forwardTimer += Time.deltaTime;
                        weapon.transform.Rotate(-swing);

                        if (forwardTimer > 0.1f)
                        {
                            forward = false;
                        }
                    }
                    else {

                        forwardTimer -= Time.deltaTime;
                        weapon.transform.Rotate(swing);

                        if (forwardTimer < 0f)
                        {
                            forward = true;
                        }

                    }

                }
                
                Destroy(fireBall, Time.deltaTime);
                
            }
        }


        //Spawning and shooting the actual fireball
        if ((Input.GetMouseButtonUp(0) ||
            (Input.GetMouseButton(0) && chargeTimer >= 0.5f && powerUpAttackSpeed))
            && coolDown <= 0 && !swinging)
        {

            weapon.transform.localRotation = origRot;

            //spawning the bullet
            var fireBall = (GameObject)Instantiate(
            fireBallPrefab,
            fireBallSpawn.position,
            fireBallSpawn.rotation
            );


            //Adjusting fireball size
            /*
            if (chargeTimer < 0.2f)
            {
                fireBall.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            }
            */

            if (chargeTimer > 0.0f && chargeTimer < 0.5f)
            {
                fireBall.transform.localScale = new Vector3(chargeTimer, chargeTimer, chargeTimer);
            }


            if (chargeTimer > 0.5f)
            {
                fireBall.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }


            //add velocity to the fireball
            if (chargeTimer < 0.2f)
            {
                fireBall.GetComponent<Rigidbody>().velocity =
                                                fireBall.transform.forward * 0.2f * 50;
                
                
            }
            if (chargeTimer > 0.2f && chargeTimer < 0.5f)
            {
                fireBall.GetComponent<Rigidbody>().velocity =
                                                fireBall.transform.forward * chargeTimer * 50;

                //Instantiate(shotSoundSrc, fireBallSpawn.position, Quaternion.identity);
            }

            if (chargeTimer > 0.5f)
            {

                fireBall.GetComponent<Rigidbody>().velocity =
                                            fireBall.transform.forward * 0.5f * 50;

                //Instantiate(shotSoundSrc, fireBallSpawn.position, Quaternion.identity);
                fireBall.tag = "SuperFireball";
            }

            Instantiate(shotSoundSrc, fireBallSpawn.position, Quaternion.identity);

            //destroying the fireball

            Destroy(fireBall, 2.0f);

            coolDown = 1 - chargeTimer;
            if (powerUpAttackSpeed) {
                coolDown = 0.1f;
            }

            chargeTimer = 0f;


            

        }
        if (Input.GetMouseButtonUp(0))
        {
            if(coolDown > 0)
                chargeTimer = 0.2f;

            Destroy(charge1);
            charge1Spawned = false;
            
        }

        if (health <= 0 && charge1 != null) {
            Destroy(charge1);
        }
    }

    //PLAYER MELEE FUNCTION
    void Melee() {

        Vector3 swing = new Vector3(3, -3, -7);

        if (powerUpAttackSpeed) {
            swing = new Vector3(3 * 2, -3 * 2, -7 * 2);
        }

        if (Input.GetKeyDown(KeyCode.F) && !swinging) {




            swinging = true;
            swingingBack = false;
            hitBox.SetActive(true);
            chargeTimer = 0;

            //play attack-sound
            int n = Random.Range(1, attackSound.Length);
            m_AudioSource.clip = attackSound[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            attackSound[n] = attackSound[0];
            attackSound[0] = m_AudioSource.clip;
        }


        if (swinging)
        {
            if (!swingingBack)
            {
                swingTimer += 1;
                weapon.transform.Rotate(swing);
                if (swingTimer == swingSpeed)
                {
                    swingingBack = true;
                }

            }
            else if (swingingBack) {
                swingTimer -= 1;
                weapon.transform.Rotate(-swing);
                if (swingTimer == 0) {
                    swinging = false;

                    swingingBack = false;
                    weapon.transform.localRotation = origRot;
                    hitBox.SetActive(false);
                }
            }


        }


    }

    public void SetPowerUp(bool b, string s) {

    }

    public void PowerUp() {
        if (powerUpAttackSpeed) {
            swingSpeed = 10;
            chargeTimer += Time.deltaTime * 2;
            powerUpTimer -= Time.deltaTime;

            if (powerUpTimer <= 0) {
                powerUpAttackSpeed = false;
                swingSpeed = 20;
            }
        }
    }

    void TakeDamage2()
    {

        if (alive)
        {
            int n = Random.Range(1, damageToPlayer.Length);
            m_AudioSource.clip = damageToPlayer[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            damageToPlayer[n] = damageToPlayer[0];
            damageToPlayer[0] = m_AudioSource.clip;
        }
    }

    void TakeDamage3()
    {

        if (alive)
        {
            int n = Random.Range(1, painSound.Length);
            m_AudioSource.clip = painSound[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            painSound[n] = painSound[0];
            painSound[0] = m_AudioSource.clip;

            var DamageWithin = GetComponentsInChildren<AudioSource>();

            foreach (AudioSource source in DamageWithin)
            {
                if (source.gameObject.GetInstanceID() != GetInstanceID())
                {
                    int m = Random.Range(1, damageToPlayer.Length);
                    source.clip = damageToPlayer[m];
                    source.PlayOneShot(source.clip);
                    // move picked sound to index 0 so it's not picked next time
                    damageToPlayer[m] = damageToPlayer[0];
                    damageToPlayer[0] = source.clip;


                }
            }


        }
    

    }

    public void TakeDamage(int i) {
        health -= i;
        _VignetteIntensity += i/3;

        //play player pain sound
        TakeDamage3();
        
        
        

        regenTimer = 3;
    }

   void RegenerationSound()
    {

        if (alive)
        {
            m_AudioSource.clip = regenSound;
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
        }

    }

    void DeathSound()
    {
        m_AudioSource.clip = deathSound;
        m_AudioSource.PlayOneShot(m_AudioSource.clip);


    }


    public void addScore(int i) {
        score += i;
        scoreText.text = "Score: " + score;
        winText.text = "Game Over!\nFinal Score: " + score
            +"\n\n(M)enu / (R)estart";
        
    }

    public void ResetPlayer() {



        if (health <= 0)
        {

            if (alive)
            {
                DeathSound();
            }
            alive = false;

            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            GetComponent<FirstPersonController>().enabled = false;
            
            timer = 3;
            winScreen.gameObject.SetActive(true);
            winScreen.CrossFadeAlpha(255, 2f, false);

            lowHealthSound.UnloadAudioData();
            Destroy(regenSound);
            Destroy(lowHealthSound);


        }
        
    }

    public void VignetteControl() {

        if(health == 3)
        {

            _VignetteIntensity = 0f;

        }else if (health == 2)
        {

            _VignetteIntensity = 0.5f;

        }else if (health == 1)
        {

            _VignetteIntensity = 1f;

            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.clip = lowHealthSound;
                m_AudioSource.PlayOneShot(m_AudioSource.clip);
            }
        }

        vignette.opacity.value = _VignetteIntensity;

        //REGENING PLAYER HEALTH
        if (health != 3) {
            regenTimer -= Time.deltaTime;
            if (regenTimer <= 0) {
                health += 1;
                //play  regeneration sound
                RegenerationSound();
                regenTimer = 3;
            }
        }

    }

    public void LoadByIndex(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

}
