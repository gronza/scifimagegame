﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaffController : MonoBehaviour {

    ParticleSystem staffAnimation;

	// Use this for initialization
	void Start () {
        GetComponentInChildren<staffpart>().gameObject.GetComponent<ParticleSystem>().Play();
        GetComponentInChildren<Canvas>().gameObject.GetComponent<ParticleSystem>().Stop();
        
    }
	
	// Update is called once per frame
	void Update () {
        

        if (Input.GetMouseButtonDown(0)) {
            GetComponentInChildren<Canvas>().gameObject.GetComponent<ParticleSystem>().Play();
            GetComponentInChildren<staffpart>().gameObject.GetComponent<ParticleSystem>().Stop();
        }
        else if(Input.GetMouseButtonUp(0))
        {
            GetComponentInChildren<staffpart>().gameObject.GetComponent<ParticleSystem>().Play();
            GetComponentInChildren<Canvas>().gameObject.GetComponent<ParticleSystem>().Stop();
        }
        
	}
}
