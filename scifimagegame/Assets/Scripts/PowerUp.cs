﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    public PlayerShoot player;

    public PowerUpSpawner HowManyPickedUp;

    

    private AudioSource m_AudioSource;

    [SerializeField] private AudioClip pickUpSound;



    void Start()
    {

        m_AudioSource = GetComponentInChildren<AudioSource>();

    }



        private void OnTriggerEnter(Collider other)
    {
        
        //Attack speed powerup
        if (other.gameObject.CompareTag("Player") && gameObject.GetComponent<MeshRenderer>().enabled)
        {
            m_AudioSource.clip = pickUpSound;
            
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            Destroy(gameObject, 1);
            HowManyPickedUp.PickedUpAmount++;
            player.powerUpTimer = 5;
            player.powerUpAttackSpeed = true;
        }
    }
}

