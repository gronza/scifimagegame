﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayShotSound : MonoBehaviour {


    AudioSource src;
	// Use this for initialization

    void Start()
    {
        src = GetComponent<AudioSource>();
        src.pitch = Random.Range(.85f, 1.25f);
        if (!src.isPlaying)
        {
            src.PlayOneShot(src.clip);
        }

        Destroy(gameObject, 1);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
