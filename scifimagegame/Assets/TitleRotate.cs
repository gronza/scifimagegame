﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleRotate : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Cursor.lockState = 0;
	}
	
	// Update is called once per frame
	void Update () {

        transform.Rotate(0, -0.1f, 0, Space.World);
		
	}
}
